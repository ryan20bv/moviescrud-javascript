const startAddMovieBtn = document.querySelector('header').lastElementChild;
// add modal elements
const addModalElement = document.getElementById('add-modal');
const cancelModalBtn = addModalElement.querySelector('.btn--passive');
const backDropElement = document.getElementById('backdrop');
const confirmAddModalBtn = addModalElement.querySelector('.btn--success');
const userInputsElements = addModalElement.querySelectorAll('input');

const movieListElement = document.getElementById('movie-list');
const editDeleteElement = document.getElementById('edit-delete-modal');

const deleteModalElement = document.getElementById('delete-modal');




let movies = [];
let selectedMovieId = "";
let isEditing = false;


// show modal for adding movie
const showModalHandler = () => {
    addModalElement.classList.add('visible');
    confirmAddModalBtn.textContent = "Add";
    if (isEditing){
        confirmAddModalBtn.textContent = "Edit";
        //console.log(selectedMovieId);
        const movieIndexToEdit = findIndexFromMovieArrayHandler(selectedMovieId);
        userInputsElements[0].value = movies[movieIndexToEdit].title;
        userInputsElements[1].value = movies[movieIndexToEdit].imageUrl;
        userInputsElements[2].value = movies[movieIndexToEdit].rating;
    }
    showBackDropHandler();
}

// close modal for adding movie
const cancelModalHandler = () => {
    addModalElement.classList.remove('visible');
    isEditing = false;
    clearUserInputs();
}

// show backDrop
const showBackDropHandler = () => {
    backDropElement.classList.add('visible');
    backDropElement.addEventListener('click', closeBackDropHandler);
    
}

// close backdrop
const closeBackDropHandler = () => {
    backDropElement.classList.remove('visible');
    closeEditDeleteModal();
    cancelModalHandler();
    closeDeleteModal();
}

// clear value at inputs
const clearUserInputs = () => {
    for (const user of userInputsElements){
        user.value = "";
        user.placeholder = "";
    }
} 
// get user Inputs
const checkUserInputs = (title, imageUrl, rating) => {
    if(title === ""){
        userInputsElements[0].placeholder = "   Required! "
    }
    if(imageUrl === ""){
        userInputsElements[1].placeholder = "   Required! "
    }
    if(rating === "" || (rating < 1 || rating > 5) ){
        if(rating < 1 || rating > 5){
            userInputsElements[2].value = "";
        }
        userInputsElements[2].placeholder = "   Enter number between 1 and 5. 1 is the lowest";
    }
}

// add movie btn

const addMovieHandler = () => {
    const titleInput = userInputsElements[0].value.trim();
    const imageUrlInput = userInputsElements[1].value.trim();
    const ratingInput = userInputsElements[2].value.trim();
    
    if( titleInput === '' ||
        imageUrlInput === '' ||
        ratingInput === '' ||
        (+ratingInput < 1 ||  // + before the operand is like parseFloat
        +ratingInput > 5)   // + before the operand is like parseFloat
    ){
        checkUserInputs(titleInput, imageUrlInput, ratingInput);
    } else if(!isEditing) {
        const newMovie = {
            id: Math.random().toString(),
            title: titleInput,
            imageUrl: imageUrlInput,
            rating: ratingInput,
        }
        movies.push(newMovie);
        renderMovieUi(newMovie.id, newMovie.title, newMovie.imageUrl, newMovie.rating);
        
    } else if(isEditing){
        //console.log("editing");
        const editedMovie = {
            id: selectedMovieId,
            title: titleInput,
            imageUrl: imageUrlInput,
            rating: ratingInput
        }

        let index = findIndexFromMovieArrayHandler(selectedMovieId);

        //console.log("index: ", index)
        //console.log(editedMovie)
        movies[index] = editedMovie;
        renderMovieUi(editedMovie.id, editedMovie.title, editedMovie.imageUrl, editedMovie.rating);
        // not done here
    }
    showUiHandler();
    clearUserInputs();
    closeBackDropHandler();
    console.log(movies);
}

const showUiHandler = () => {
    const entryText = document.getElementById('entry-text');
    if(movies.length === 0){
        entryText.style.display = 'block';
    } else{
        entryText.style.display = 'none';
    }
}

const renderMovieUi = (id, title, imageUrl, rating)=> {
    const newMovie = document.createElement('li');
    newMovie.innerHTML = `
            <div class = "movie-element">
                <img src="${imageUrl}" alt="${title}" class= "movie-element__image" >
                <div class= "movie-element__info">
                    <h2>${title}</h2>
                    <p>${rating}/5 stars</p>
                </div>
            </div>
        `;
    if(!isEditing){
        movieListElement.insertAdjacentElement("beforeend", newMovie);
    } else if(isEditing){
        let index = findIndexFromMovieArrayHandler(id);
        movieListElement.children[index].replaceWith(newMovie);
    }
    
    newMovie.addEventListener('click', showEditDeleteModal.bind(null, id));
} 


const showEditDeleteModal = (id) => {
    editDeleteElement.classList.add('visible');
    showBackDropHandler()
    const movieIndex = findIndexFromMovieArrayHandler(id);
    const h2 = editDeleteElement.querySelector('h2');
    const cancelEditDeleteModalBtn = editDeleteElement.querySelector('.btn--passive');
    const deleteEditDeleteModalBtn = editDeleteElement.querySelector('.btn--danger');
    const editEditDeleteModalBtn = editDeleteElement.querySelector('.btn--success');
    h2.textContent = `Movie Title: ${movies[movieIndex].title}`;
    //console.log('show',id)
    selectedMovieId = id;
    cancelEditDeleteModalBtn.addEventListener('click', closeBackDropHandler);
    deleteEditDeleteModalBtn.addEventListener('click', showDeleteModal);
    editEditDeleteModalBtn.addEventListener('click', editMovieHandler);

}

const findIndexFromMovieArrayHandler = (id) => {
    let index = 0;
    for (const movie of movies){
        if(movie.id === id){
            break;
        }
        index++;
    }
    return index;
}

// this is trail.... no id , title, content
const findIndexFromMovieListElementHandler = (id) => {
    let index = 0;
    for(const list of movieListElement){
        if(list.id === id){
            break;
        }
        index++;
    }
    return index;
}

const closeEditDeleteModal = () => {
    editDeleteElement.classList.remove('visible');
}

const clearSelectedMovieId = () => {
    selectedMovieId = "";
}

const showDeleteModal = () => {
    const movieToDeleteId = selectedMovieId;
    const movieToDeleteIndex = findIndexFromMovieArrayHandler(movieToDeleteId)
    closeEditDeleteModal();
    //console.log(`selected id: ${selectedMovieId}, id: ${movieToDeleteId}`)
    deleteModalElement.classList.add('visible');
    const cancelDeleteModalBtn = deleteModalElement.querySelector('.btn--passive');
    const confirmDeleteModalBtn = deleteModalElement.querySelector('.btn--danger');
    const span = deleteModalElement.querySelector('span');
    span.textContent = `Movie Title: ${movies[movieToDeleteIndex].title}`;
    span.style.color = "red";
    cancelDeleteModalBtn.addEventListener('click', closeBackDropHandler);
    confirmDeleteModalBtn.addEventListener('click', confirmDeleteHandler)
}

const closeDeleteModal = () => {
    deleteModalElement.classList.remove('visible');
}

const confirmDeleteHandler = () => {
    //console.log(selectedMovieId);
    const index = findIndexFromMovieArrayHandler(selectedMovieId);
    movieListElement.children[index].remove();
    movies.splice(index,1); // removed from movies list
    //console.log("confirmed delete");
    console.log(movies);
    showUiHandler();
    closeBackDropHandler();
    clearSelectedMovieId();
}

// next task is Edit
const editMovieHandler = () => {
    isEditing = true;
    showModalHandler();
    closeEditDeleteModal();
    showBackDropHandler();
}


startAddMovieBtn.addEventListener('click', showModalHandler);
cancelModalBtn.addEventListener('click', closeBackDropHandler);
confirmAddModalBtn.addEventListener('click', addMovieHandler);

//success finished